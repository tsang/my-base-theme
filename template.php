<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728096
 */


/**
 * Override or insert variables into the html templates. We use this to apply some of our meta tag customisations
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function kci_base_theme_preprocess_html(&$variables, $hook) {
  // Site title customisations, if any:
  $site_title_elements      = theme_get_setting('kci_site_title_elements');
  $site_title_override      = theme_get_setting('kci_site_title_name_override');
  $site_title_home_override = theme_get_setting('kci_site_title_name_home_override');
  $site_slogan_override     = theme_get_setting('kci_site_title_slogan_override');
  $site_title_separator     = trim(theme_get_setting('kci_site_title_separator'));
  if (!empty($site_title_separator)) {
    $site_title_separator = ' ' . $site_title_separator . ' ';
  } else {
    $site_title_separator = ' | ';
  }
  if (!empty($site_title_elements)) {
    // Replace the head title array with only the elements we want to keep:
    $head_title_array = array();
    foreach ($site_title_elements as $element_name) {
      if (!empty($variables['head_title_array'][$element_name])) {
        $head_title_array[$element_name] = $variables['head_title_array'][$element_name];
      }
    }
    $variables['head_title_array'] = $head_title_array;
  }
  if (drupal_is_front_page()) {
    if (!empty($site_title_home_override) && !empty($variables['head_title_array']['name'])) {
      $variables['head_title_array']['name'] = $site_title_home_override;
    }
  } else {
    if (!empty($site_title_override) && !empty($variables['head_title_array']['name'])) {
      $variables['head_title_array']['name'] = $site_title_override;
    }
  }
  if (!empty($site_slogan_override) && !empty($variables['head_title_array']['slogan'])) {
    $variables['head_title_array']['slogan'] = $site_slogan_override;
  }
  $variables['head_title'] = implode($site_title_separator, $variables['head_title_array']);
  // OpenGraph metatags:
  $enabled_og_tags = theme_get_setting('kci_enabled_og_metatags');
  if (!empty($enabled_og_tags)) {
    if (in_array('title', $enabled_og_tags)) {
      $variables['kci_og_tag_title'] = $variables['head_title'];
    }
    if (in_array('url', $enabled_og_tags)) {
      $path_alias = drupal_get_path_alias();
      if ($path_alias == 'node') {
        $path_alias = '';
      }
      $variables['kci_og_tag_url'] = url($path_alias, array('absolute' => true));
    }
    if (in_array('image', $enabled_og_tags)) {
      global $theme;
      $path = drupal_get_path('theme', $theme);
      $og_image_paths = array(
        $path . '/images/og-preview.jpg',
        $path . '/images/og-preview.jpeg',
        $path . '/images/og-preview.gif',
        $path . '/images/og-preview.png',
      );
      $og_image_path = '';
      foreach ($og_image_paths as $image_path) {
        if (file_exists(DRUPAL_ROOT . '/' . $image_path)) {
          $og_image_path = $image_path;
          break;
        }
      }
      if (!empty($og_image_path)) {
        $variables['kci_og_tag_image'] = url($og_image_path, array('absolute' => true));
      }
    }
  }
}
