<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>

<div id="page">

    <?php
    $secondary_menu = theme('links__system_secondary_menu', array(
      'links' => $secondary_menu,
      'attributes' => array(
        'class' => array('menu', 'inline', 'clearfix'),
      ),
    ));
    $top_bar = render($page['top_bar']);
    if ($secondary_menu || $top_bar): ?>
    <section id="top-bar">
      <div class="container">

        <?php if ($secondary_menu): ?>
          <nav class="header__secondary-menu" role="navigation">
            <?php print $secondary_menu; ?>
          </nav>
        <?php endif; ?>

        <?php print $top_bar; ?>

      </div>
    </section>
    <?php endif; ?>

    <header id="header" class="header" role="banner">
      <div class="container">

        <?php if ($logo || $site_name || $site_slogan): ?>
        <div id="branding">
          <?php if ($logo): ?>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo-image" /></a>
          <?php endif; ?>

          <?php if ($site_name || $site_slogan): ?>
            <div class="header__name-and-slogan <?php if($logo): ?>header__with-logo<?php endif; ?> <?php if($site_slogan): ?>header__with-slogan<?php endif; ?>" id="name-and-slogan">
              <?php if ($site_name || $site_slogan): ?>
                <h1 class="header__site-name" id="site-name">
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="header__site-link" rel="home">
                    <span><?php print $site_name; ?></span>
                  </a>
                  <?php if ($site_slogan): ?>
                    <span class="header__site-slogan" id="site-slogan"><?php print $site_slogan; ?></span>
                  <?php endif; ?>
                </h1>
              <?php endif; ?>

            </div>
          <?php endif; ?>
          </div>
        <?php endif; ?>

        <?php print render($page['header']); ?>

        <a id="nav-button" href="">Open Menu</a>

      </div>
    </header>

    <section id="main">
      <div class="container">

        <?php
        // Render the icing that goes on top of the cake. This is our full-width region above the content that we can
        // use for things like a full-width slider that is not appropriate to put in the header or navigaiton regions
        $icing = render($page['icing']);
        if ($icing):
        ?>
        <div id="icing" class="clearfix"><?php print $icing; ?></div>
        <?php endif; ?>

        <div id="content" class="column" role="main">
          <div class="content-inner">
            <?php print render($page['highlighted']); ?>
            <?php print $breadcrumb; ?>
            <a id="main-content"></a>
            <?php print render($title_prefix); ?>
            <?php if ($title): ?>
              <h1 class="page__title title" id="page-title"><?php print $title; ?></h1>
            <?php endif; ?>
            <?php print render($title_suffix); ?>
            <?php print $messages; ?>
            <?php print render($tabs); ?>
            <?php print render($page['help']); ?>
            <?php if ($action_links): ?>
              <ul class="action-links"><?php print render($action_links); ?></ul>
            <?php endif; ?>
            <?php print render($page['content']); ?>
            <?php print $feed_icons; ?>
          </div>
        </div>

        <?php
          // Render the sidebars to see if there's anything in them.
          $sidebar_first  = render($page['sidebar_first']);
          $sidebar_second = render($page['sidebar_second']);
        ?>

        <?php if ($sidebar_first || $sidebar_second): ?>
          <aside class="sidebars container">
            <?php print $sidebar_first; ?>
            <?php print $sidebar_second; ?>
          </aside>
        <?php endif; ?>

      </div>
    </section>

    <?php
    $main_menu = theme('links__system_main_menu', array(
      'links' => $main_menu,
      'attributes' => array(
      'class' => array('menu', 'clearfix'),
      ),
    ));
    $navigation = render($page['navigation']);
    // Checks for main menu or navigation region blocks
    if ($main_menu || $navigation): ?>
    <nav id="navigation" role="navigation" tabindex="-1" class="<?php if (!$secondary_menu && !$top_bar): ?>no-top-bar<?php endif; ?>">
      <div class="container">

        <?php if (!$navigation): ?>
          <div id="main-menu">
            <?php print $main_menu; ?>
          </div>
        <?php endif; ?>

        <?php print $navigation; ?>

      </div>
    </nav>
    <?php endif; ?>

    <?php
    $footer = render($page['footer']);
    if ($footer): ?>
    <footer id="footer">
      <div class="container">
        <?php print $footer; ?>
      </div>
    </footer>
    <?php endif; ?>

</div>
<!-- /#page -->

<?php print render($page['bottom']); ?>
